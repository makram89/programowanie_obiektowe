#pragma once
#include "Serial.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include "film.h"
#include <algorithm>
#include <iomanip>
#include <ctime>
#include "Live.h"
#include "FileError.h"
#include "Universal.h"
using namespace std;

template <class T>
class Baza_programow
{
private:
	vector<T> baza;

public:
	Baza_programow() {};
	~Baza_programow() {};
	void edit();
	void kreator_pozycji(); //string, string, int,int);
	void wyswietlenie_bazy();
	void odczyt_bazy_seriali(string);
	void zapis_bazy(string);//fstream &);

	void Interfejs(){
		cout << "(s) Show database" << endl;
		cout << "(a) Add record" << endl;
		cout << "(e) Edit record" << endl;
		cout << "(r) Get recomendation" << endl;
		cout << "(q) Quit" << endl;
	};
	
	
	void menu_baza(string adres);
	bool sprawdzenie_zakresu(int, int, int);
	void rekomendacja() {
		if (rand() % 2) { baza[rand() % baza.size()].show_s(); cout << endl;
		}
		else { baza[0].show_s(); cout << endl; }
	} //losuje liczbe  z zakresu 0-1 jesli 0 to najlepsza opcja jesli 1 to losowa wartos
	void operator+=(T typ) { baza.push_back(typ); }
	void operator-=(int a) { baza.erase(baza.begin() + a); } //tak musi by�, ustawiamy na poczatek i przes�wamy
	void sortowanie();
};


template <class T>
void Baza_programow<T>::edit()
{
	char c;
	bool flag = true;
	while (flag)
	{
		cout << "You want to edit or delete position? e/d Type 'q' to quit." << endl;
		cin.clear();
		cin >> c;
		if (c == 'e')
		{

			cout << "Give index of positon you want to edit" << endl;
			int a;
			while (true)
			{
				cin.clear();
				cin >> a;
				if (sprawdzenie_zakresu(a, 0, baza.size())) break;
				else cout << "Wrong index, try agian" << endl;
			}
		
			baza[a].edit_pos();
			flag = false;
		}
		if (c == 'd')
		{
			cout << "Give index of positon you want to delete" << endl;
			int a;
			while (true)
			{
				cin.clear();
				cin >> a;
				if (sprawdzenie_zakresu(a, 0, baza.size())) break;
				else cout << "Wrong index, try agian" << endl;
			}

			*this -= a;
			flag = false;
		}
		if (c == 'q')
		{
			flag = false;
		}
	}


}


template <class T>
void Baza_programow<T>::kreator_pozycji()
{
	T S_Serial;				//Wystapienie S_Serial typu T
	S_Serial.Create();
	//baza.push_back(S_Serial);  //pusz_back -> metoda kllasy vektor
	*this += S_Serial;
}

template <class T>
void Baza_programow<T>::wyswietlenie_bazy()
{
	
	if (baza.size() != 0) {
		baza[0].naglowek();	
		cout << endl;
		for (size_t i = 0; i < baza.size(); i++)
		{
			cout.width(0);
			cout << i << "\t";
			baza[i].show_s();	
			cout << endl;
		}
	}
	else throw FileOpenError("ERROR No objects in base");
}

template <class T>
void Baza_programow<T>::odczyt_bazy_seriali(string adres) //odczyt z pliku
{
	T Rekord;
	string Linia;
	string Slowo;
	fstream Plik;
	string::size_type type;
	baza.clear(); // potrzebne zeby czyiscic kontener i nie pokazywal tej samje bazy dwa razy
	baza.resize(0);
	Plik.open(adres, ios::in); //TODO Dodac obs�ug� b��d�w W1. plik
	if (Plik.good() == true)
	{

		while (!Plik.eof())
		{
			getline(Plik, Linia);
			stringstream StringStream(Linia); //Zamiana na strumien zeby uzyc getlina()
			for (int i = 0; !StringStream.eof() && i <= 4; i++) ///TODO obsluga wsyztskich p�l, 
			{
				getline(StringStream, Slowo, ',');
				
				//Slowo.erase(remove_if(Slowo.begin(), Slowo.end(), isspace), Slowo.end()); //Usuwanie spacji
				switch (i)
				{
				case 0:
					Rekord.set_Name(Slowo);
					break;
				case 1:
					Rekord.set_Gatunek(Slowo);
					break;
				case 2:
					Rekord.set_Ocena(stoi(Slowo, &type));
					break;
				case 3:
					//TODO nic
					break;
				case 4:
					
					Rekord.set_time(stoll(Slowo,&type));
					break;
				default:
					break;
				}
			}
			if (Slowo.empty()) break;
			else baza.push_back(Rekord);
		}
		Plik.close();
	}
	else throw FileOpenError("Openig database error, check if file " + adres + " exist, and it`s not in use. \n If its something else please contact with someone.  \n"); ///TODO b��d otwarcia bazy tu miejsce na wyjatki
}


template <class T>
void Baza_programow<T>::zapis_bazy(string s_Adres)//fstream &Plik) //zapis do pliku
{
	fstream plik;
	plik.open(s_Adres, ios::trunc | ios::out);
	if (plik.good() == true)
	{
		for (int i = 0; i < baza.size(); i++)
		{
			//TODO Dodac staty
			plik << baza[i].save();

		}
	}
	else cout << "ERROR saving error";
	plik.close();
}


template<class T>
 void Baza_programow<T>::menu_baza(string A_Seriali)
{
	 bool flag = true;
	 char C;
	 while (flag)
	 {
		 system("cls");
		 Interfejs();
		 cin.sync();
		 cin >> C;
		 cin.sync();
		 cin.ignore(1000, '\n');
		 cin.clear();
		 switch (C)
		 {
		 case 's':   //obsluga showa
			 system("cls");
			 wyswietlenie_bazy();
			 printf("\n Press enter to get back \n");
			 system("pause");
			 break;
		 case 'a':   //obsluga add
			 system("cls");
			 kreator_pozycji();
			 zapis_bazy(A_Seriali);
			 break;

		 case 'e':	//obsluga edit
			 system("cls");
			 wyswietlenie_bazy();
			 edit();
			 zapis_bazy(A_Seriali);
			 break;

		 case 'r': //obluga rekomendacja 
			 sortowanie();
			 zapis_bazy(A_Seriali);
			 cout << "Recomended show" << endl;
			 rekomendacja();
			 system("pause");
			 break;
		 case 'q': //obluga exit
			 flag = false;
			 break;

		 default:
			 cin.sync();
			 break;
		 }

	 }
}

template <class T>
bool Baza_programow<T>::sprawdzenie_zakresu(int x, int L, int P)
{
	if (x >= L && (x < P))
	{
		return true;
	}
	else return false;
}

template<class T>
void Baza_programow<T>::sortowanie()
{
	sort(baza.begin(), baza.end());
	
}
