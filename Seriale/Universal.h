
#pragma once
#include <string>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include "FileError.h"
using namespace std;

class Universal
{

private:
	string Name; 
	string Genere; //POMYSL mo�a spr�bowa� stworzy� enum {Komedia,itd}.
	int Ocena;

	/// zamienic na klase uniwersalna i dodac na nowo serial
	/// Generecznie menu, albo spr�bowa� jedn� metode

public:
	Universal() {};
	string get_Name() { return Name; }
	string get_Genere() { return Genere; }
	int get_Ocena() { return Ocena; }
	void show_s();
	string save();
	//void naglowek() {};
	void set_Name(string);
	void set_Gatunek(string);
	void set_Ocena(int);
	void Create();
	void edit_pos();
	void proc_Name();
	void proc_Genere();
	void proc_Ocena();
	bool operator<(Universal A) { return this->Name < A.Name; } // Teraz uklada nazwe alfabetycznie
	virtual void set_time(int) {};
	bool sprawdzenie_zakresu(int sprawdzana, int lewa, int prawa);
};


