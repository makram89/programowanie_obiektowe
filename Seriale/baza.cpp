#pragma once
#include "Serial.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include "film.h"
#include <algorithm>

using namespace std;

template <class T>
class baza_programow
{
private:
	vector<T> Baza;

public:
	baza_programow();
	~baza_programow();
	void edit();
	void kreator_pozycji(); //string, string, int,int);
	void wyswietlenie_bazy();
	void odczyt_bazy_seriali();
	void zapis_bazy_seriali(fstream &);

	
	
};

template <class T>
baza_programow<T>::baza_programow()
{
}

template <class T>
baza_programow<T>::~baza_programow()
{
}

template <class T>
void baza_programow<T>::edit()
{
	cout << "Wybierz pozycje" << endl;
	int a;
	cin >> a;
	Baza[a].edit_pos();

}


template <class T>
void baza_programow<T>::kreator_pozycji()
{
	T S_Serial;				//Wystapienie S_Serial typu T
	S_Serial.Create(); 
	Baza.push_back(S_Serial);  //pusz_back -> metoda kllasy vektor
}

template <class T>
void baza_programow<T>::wyswietlenie_bazy()
{
	Baza[0].naglowek();
	cout << Baza.size() << endl << "BAZASIZE UP" << endl;
	for (size_t i = 0; i < Baza.size(); i++)
		cout<<Baza[i].show_s();

}

template <class T>
void baza_programow<T>::odczyt_bazy_seriali() //odczyt z pliku
{
	T Rekord;
	string Linia;
	string Slowo;
	fstream Plik;
	string::size_type type;
	Baza.clear(); // potrzebne zeby czyiscic kontener i nie pokazywal tej samje bazy dwa razy
	Baza.resize(0);
	Plik.open("plik1.txt", ios::in); //TODO Dodac obs�ug� b��d�w W1. plik
	if (Plik.good() == true)
	{

		while (!Plik.eof())
		{
			getline(Plik, Linia);
			stringstream StringStream(Linia);
			for (int i = 0; !StringStream.eof() && i < 5; i++) //TODO obsluga wsyztskich p�l, na razue tylko pierwsze 3(i)
			{
				getline(StringStream, Slowo, ',');
				Slowo.erase(remove_if(Slowo.begin(), Slowo.end(), isspace), Slowo.end());
				switch (i)
				{
				case 0:
					Rekord.set_Name(Slowo);
					break;
				case 1:
					Rekord.set_Gatunek(Slowo);
					break;
				case 2:
					Rekord.set_Ocena(stoi(Slowo, &type));
					break;
				default:
					break;
				}
			}
			Baza.push_back(Rekord);
		}
		Plik.close();
	}
	else cout << "ERROR Otwarcie bazy";
}

template <class T>
void baza_programow<T>::zapis_bazy_seriali(fstream &Plik) //zapis do pliku
{
	
	
	if (Plik.good() == true)
	{
		for (int i = 0; i < Baza.size(); i++)
		{
			Plik << Baza[i].show_s();
		}
	}
	
}